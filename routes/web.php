<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('/products');
    // return view('welcome');
});

Route::group(['middleware' => 'auth'], function(){
  Route::get('/myCart', 'ProductController@myCart');
  Route::get('/product/details/{id}', 'ProductController@singleItemDetails' );
  Route::post('/confirmOrder', 'ProductController@confirmOrder' );

  // Route::get('/payWithRazorPay', 'RazorpayCOntroller');
});

Route::get('/products', 'ProductController@index' );
Route::get('/myOrders', 'ProductController@myOrders' );
Route::get('/viewOrder/{bill_no}', 'ProductController@viewOrder' );
Route::get('/cart/addToCart/{id}', 'ProductController@addToCart' );
Route::get('/cart/deleteFromCart/{id}', 'ProductController@deleteFromCart' );
Route::get('/changeBillStatusToPaid/{bill_no}/{payment_id}/', 'ProductController@changeBillStatusToPaid');
Auth::routes();

Route::get('/home', 'ProductController@index')->name('home');
Route::get('/dashboard', function(){
  return redirect('/products');
});

// route for to show payment form using get method
Route::get('pay', 'RazorpayController@pay')->name('pay');

// route for make payment request using post method
Route::post('dopayment', 'RazorpayController@dopayment')->name('dopayment');
