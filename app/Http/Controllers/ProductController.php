<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\CartProduct;
use Auth;
use App\Invoice;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::get();
        if (Auth::check()) {
          $cartCount = CartProduct::where('created_by', Auth::user()->id)->count();
        } else {
          $cartCount = '';
        }

        // dd($products);
        return view('products.list')->with([
          'products'=>$products,
          'cartCount' => $cartCount,
        ]);
    }

    public function addToCart($pid)
    {
      $cartItem = new CartProduct();
      $cartItem->created_by = Auth::user()->id;
      $cartItem->product_id = $pid;
      $cartItem->save();

      return back();
    }

    public function singleItemDetails($pid)
    {
      $product = Product::where('id', $pid)->first();
      $cartCount = CartProduct::where('created_by', Auth::user()->id)->count();
      // dd($product);

      return view('products.details')->with([
        'product'=>$product,
        'cartCount' => $cartCount,
      ]);
    }

    public function myCart()
    {
      $product = [];
      $cartProductIds = [];
      $products = [];

      $cartProducts = CartProduct::where('created_by', Auth::user()->id)->get()->toArray();

      foreach ($cartProducts as $cartProduct) {

          $cartProductIds[] = $cartProduct['product_id'];
      }

      $cProducts = Product::whereIn('id', $cartProductIds)->get()->toArray();

      foreach ($cProducts as $key => $product) {
          $products[$product['id']] = $product;
      }

      return view('myCart')->with([
        'cartCount' => count($cartProductIds),
        'cartProducts'=> $cartProducts,
        'products' => $products,
        'cartProductIds' => $cartProductIds,
        'json' => json_encode($cartProductIds),
      ]);
    }

    public function deleteFromCart($cpid)
    {
        CartProduct::where('id', $cpid)->delete();

        return back();
    }

    public function confirmOrder(Request $request)
    {
      // dd($request->all());
      $bill = new Invoice();
      $bill->bill_no = Auth::user()->id.'_'.date('Ymdhis');
      $bill->status = 'Unpaid';
      $bill->created_by = Auth::user()->id;
      $bill->product_json = $request['product_json'];
      $bill->amount = $request['amount'];
      $bill->save();

      CartProduct::where('created_by', Auth::user()->id)->delete();

      return redirect('/viewOrder/'.$bill->bill_no);

    }

    public function myOrders()
    {
      $myOrders = Invoice::where('created_by', Auth::user()->id)->get();
      $cartProducts = CartProduct::where('created_by', Auth::user()->id)->get()->toArray();

      // dd($myOrders);

      return view('myOrders')->with([
        'cartCount' => count($cartProducts),
        'orders' => $myOrders,
      ]);
    }

    public function viewOrder($bill_no)
    {
      $cartProducts = CartProduct::where('created_by', Auth::user()->id)->get()->toArray();

      $order = Invoice::where('bill_no', $bill_no)->first()->toArray();

      $cartProductIds = json_decode($order['product_json']);

      $cProducts = Product::whereIn('id', $cartProductIds)->get()->toArray();

      foreach ($cProducts as $key => $product) {
          $products[$product['id']] = $product;
      }
      // dd($cartProductIds);
      return view('viewOrder0')->with([
        'cartCount' => count($cartProducts),
        'cartProductIds' => $cartProductIds,
        'products' => $products,
        'order' => $order,
      ]);
    }

    public function changeBillStatusToPaid($bill_no, $payment_id)
    {
      $invoice = Invoice::where('bill_no', $bill_no)->first();
      $invoice->status = 'Paid';
      $invoice->payment_id = $payment_id;
      $invoice->payment_date = date('d.m.Y H:i');
      $invoice->save();

      $data = 'Paid';

      return response()->json([
    'statusCode'=>200,
    'status'=>'Success',
    'message'=>'Paid',
    'success'=>$data
    ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
