<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CartProduct;

class DashboardController extends Controller
{
    //
    public function index()
    {
    	// dd('here');
    	 if (Auth::check()) {
          $cartCount = CartProduct::where('created_by', Auth::user()->id)->count();
        } else {
          $cartCount = '';
        }

      return view('products.dashboard')->with([
          'cartCount' => $cartCount,
        ]);;
    }
}
