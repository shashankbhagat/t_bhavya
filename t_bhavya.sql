-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2019 at 05:22 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `t_bhavya`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `bill_no` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `product_json` text,
  `amount` varchar(255) DEFAULT NULL,
  `payment_id` text,
  `payment_date` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `created_at`, `updated_at`, `deleted_at`, `bill_no`, `status`, `created_by`, `product_json`, `amount`, `payment_id`, `payment_date`) VALUES
(1, '2019-05-24 02:53:52', '2019-05-24 03:00:06', NULL, '1_20190524082352', 'Paid', 1, '[1,1,3]', '3', 'pay_CZCtelZ5XxV2ph', '24.05.2019 08:30'),
(2, '2019-05-24 03:03:11', '2019-05-24 03:03:11', NULL, '1_20190524083311', 'Unpaid', 1, '[2]', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `mrp` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `inStock` tinyint(4) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `description` text,
  `brand` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `status`, `name`, `image`, `mrp`, `discount`, `inStock`, `quantity`, `description`, `brand`) VALUES
(1, '2019-05-11 07:44:16', '2019-05-11 07:44:16', '2019-05-11 07:44:16', 1, 1, 'Watch', '/image/watch.jpg', '5000', '1000', 1, 10, 'akes it a simple, versatile timepiece great for everyday wear. The watch is constructed with a high quality metal case, a stationary bezel, and a comfortable strap which is easily adjustable. A durable mineral window protects the dial face, which clean features, hour index, minute markers, and complementary watch hands. Ultra quiet and low maintenance, and with modern, casual and fashion appearance. Casual enough for every day wear, this watch also looks great with business attire.', 'Fasttrack'),
(2, '2019-05-11 07:44:16', '2019-05-11 07:44:16', '2019-05-11 07:44:16', 1, 1, 'Laptop', '/image/laptop.jpg', '40000', '5000', 1, 15, 'Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration ', 'Dell'),
(3, '2019-05-11 08:17:26', '2019-05-11 08:17:26', '2019-05-11 08:17:26', 1, 1, 'Samsung Galaxy Mobile', '/image/samsung Mobile.jpg', '10000', '1000', 1, 3, '5\" HD Screen; 1.35 GHz Quad-Core processor; Android 8.0 Oreo;\r\n4G LTE; Wi-Fi Capable; Bluetooth 4.2 wireless technology; MP3 Player\r\n8 MP Camera/5 MP Front Facing Camera; Internal memory 16 GB; supports Micro SD memory card up to 400 GB (not included)\r\nPlans sold separately. Hearing Aid Compatible (HAC). Most GSM and CDMA smartphones are compatible\r\nNote: This phone is carrier locked; Customers must have had their locked device activated on service for no fewer than 12 months, redeemed air time cards in no fewer than 12 months, and not have had their telephone number recycled or ported.', 'Samsung'),
(4, '2019-05-13 05:23:13', '2019-05-13 05:23:13', '2019-05-13 05:23:13', 1, 1, 'Headphones', '/image/headphones.jpg', '2000', '300', 1, 2, 'IMPRESSIVE SOUND QUALITY IS THE ULTIMATE GOAL: The High-fidelity stereo sound benefits from the 40mm neodymium driver, CSR chip, and the around-ear cushion design which provide a well-closed and immersed environment for your ears, Just lose yourself in the music! NOTE: Mpow 059 headphones is passive noise isolating, NOT active noise cancellation(ANC), it can\'t cancel the noise completely but it won\'t drain the battery and damage the sound. 2. The closed-back design provides immersive Hi-Fi sound with CSR chip and 40mm driver together, it is better than ANC in term of sounds quality.\r\n', 'MPOW'),
(5, '2019-05-13 05:31:25', '2019-05-13 05:31:25', '2019-05-13 05:31:25', 1, 1, 'Apple iPad', '/image/apple ipad.jpg', '27500', '1800', 1, 4, '9. 7-inch Retina display\r\nA10 Fusion chip\r\nTouch ID fingerprint sensor\r\n8MP back camera and 1. 2MP FaceTime HD front camera\r\nTwo speaker audio\r\n802. 11ac Wi-Fi and LTE cellular data\r\nUp to 10 hours of battery life\r\nLightning connector for charging and accessories\r\niOS 12 with Group FaceTime, shared augmented reality experiences, Screen Time, and more\r\n', 'Apple'),
(6, '2019-05-13 05:53:18', '2019-05-13 05:53:18', '2019-05-13 05:53:18', 1, 1, 'ScanDisk 16GB USB Drive', '/image/pendrive.jpg', '700', '200', 1, 8, 'Premium, reliable and secure storage for your videos, music, photos, or other files\r\nProtect your sensitive files - SanDisk SecureAccess software included\r\nPassword protection and 128-bit AES encryption\r\nShips in Certified Frustration-Free Packaging', 'ScanDisk'),
(7, '2019-05-13 06:47:48', '2019-05-13 06:47:48', '2019-05-13 06:47:48', 1, 1, 'Induction Stove', '/image/induction stove.jpg', '3250', '360', 1, 5, 'Polished A-grade Crystal Plate Surface; LED Large Screen Display, 4 digits; Built-in Digital 3-Hour Timer setting; Touch Control Panel\r\n8 Power levels from 300 to 1800 Watts: 300w, 500w, 700w, 1000w, 1200w, 1400w, 1600w, 1800w\r\n8 Temperature Settings from 150 to 450 °: 150 °, 200 °, 260 °, 300 °, 350 °, 400 °, 425 °, 450 °\r\nNew plate top pattern design; Included 18-8 10” 3.5 Qt Stainless Steel Pot\r\nCookware Suitability Detection, Overheating Protection function\r\n', 'RoseWill'),
(8, '2019-05-13 07:18:13', '2019-05-13 07:18:13', '2019-05-13 07:18:13', 1, 1, 'Coffee Maker', '/image/coffee maker.jpg', '5900', '720', 1, 3, 'Brews multiple K-Cup Pod sizes: (6, 8, 10 oz.) – The most popular K-Cup Pod brew sizes. Use the 6oz brew size to achieve the strongest brew.\r\nLarge 48 oz. Water reservoir: allows you to brew 6+ cups before having to refill, saving you time and simplifying your morning routine. The water reservoir is removable, making it easy to refill whenever you need to.\r\nSimple button controls: just insert a Pod, select your desired brew size, and brew a fresh, great-tasting cup in under a minute. Brew Time: Less than a minute\r\nDescaling: an important part of cleaning your Keurig brewer. This process helps to remove calcium deposits, or scale, that can build up inside a Coffee maker over time. Please refer to our descaling video for step by step instructions.\r\nAuto-off: an auto-off feature is easily programmed to turn off your Coffee maker after it has been idle for two hours, helping to save energy.', 'KEURIG'),
(9, '2019-05-13 07:23:27', '2019-05-13 07:23:27', '2019-05-13 07:23:27', 1, 1, 'Double Door Compact Refrigerator', '/image/fridge1.jpg', '10150', '1200', 1, 3, '3.1 cu.ft. total capacity\r\n87 Liters independent freezer section\r\n1 full width tempered glass shelf for storage, steel door\r\nIntegrated door shelving with tall bottle storage\r\nDimensions 18.87 x 19.68 x 33.43 Inches', 'LG');

-- --------------------------------------------------------

--
-- Table structure for table `products_copy`
--

CREATE TABLE `products_copy` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `mrp` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `inStock` tinyint(4) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `description` text,
  `brand` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_copy`
--

INSERT INTO `products_copy` (`id`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `status`, `name`, `image`, `mrp`, `discount`, `inStock`, `quantity`, `description`, `brand`) VALUES
(1, '2019-05-11 07:44:16', '2019-05-11 07:44:16', '2019-05-11 07:44:16', 1, 1, 'Watch', '/image/watch.jpg', '1', '1000', 1, 10, 'akes it a simple, versatile timepiece great for everyday wear. The watch is constructed with a high quality metal case, a stationary bezel, and a comfortable strap which is easily adjustable. A durable mineral window protects the dial face, which clean features, hour index, minute markers, and complementary watch hands. Ultra quiet and low maintenance, and with modern, casual and fashion appearance. Casual enough for every day wear, this watch also looks great with business attire.', 'Fasttrack'),
(2, '2019-05-11 07:44:16', '2019-05-11 07:44:16', '2019-05-11 07:44:16', 1, 1, 'Laptop', '/image/laptop.jpg', '1', '5000', 1, 15, 'Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration Best laptop on the world with all configuration ', 'Dell'),
(3, '2019-05-11 08:17:26', '2019-05-11 08:17:26', '2019-05-11 08:17:26', 1, 1, 'Samsung Galaxy Mobile', '/image/samsung Mobile.jpg', '1', '1000', 1, 3, '5\" HD Screen; 1.35 GHz Quad-Core processor; Android 8.0 Oreo;\r\n4G LTE; Wi-Fi Capable; Bluetooth 4.2 wireless technology; MP3 Player\r\n8 MP Camera/5 MP Front Facing Camera; Internal memory 16 GB; supports Micro SD memory card up to 400 GB (not included)\r\nPlans sold separately. Hearing Aid Compatible (HAC). Most GSM and CDMA smartphones are compatible\r\nNote: This phone is carrier locked; Customers must have had their locked device activated on service for no fewer than 12 months, redeemed air time cards in no fewer than 12 months, and not have had their telephone number recycled or ported.', 'Samsung'),
(4, '2019-05-13 05:23:13', '2019-05-13 05:23:13', '2019-05-13 05:23:13', 1, 1, 'Headphones', '/image/headphones.jpg', '1', '300', 1, 2, 'IMPRESSIVE SOUND QUALITY IS THE ULTIMATE GOAL: The High-fidelity stereo sound benefits from the 40mm neodymium driver, CSR chip, and the around-ear cushion design which provide a well-closed and immersed environment for your ears, Just lose yourself in the music! NOTE: Mpow 059 headphones is passive noise isolating, NOT active noise cancellation(ANC), it can\'t cancel the noise completely but it won\'t drain the battery and damage the sound. 2. The closed-back design provides immersive Hi-Fi sound with CSR chip and 40mm driver together, it is better than ANC in term of sounds quality.\r\n', 'MPOW'),
(5, '2019-05-13 05:31:25', '2019-05-13 05:31:25', '2019-05-13 05:31:25', 1, 1, 'Apple iPad', '/image/apple ipad.jpg', '1', '1800', 1, 4, '9. 7-inch Retina display\r\nA10 Fusion chip\r\nTouch ID fingerprint sensor\r\n8MP back camera and 1. 2MP FaceTime HD front camera\r\nTwo speaker audio\r\n802. 11ac Wi-Fi and LTE cellular data\r\nUp to 10 hours of battery life\r\nLightning connector for charging and accessories\r\niOS 12 with Group FaceTime, shared augmented reality experiences, Screen Time, and more\r\n', 'Apple'),
(6, '2019-05-13 05:53:18', '2019-05-13 05:53:18', '2019-05-13 05:53:18', 1, 1, 'ScanDisk 16GB USB Drive', '/image/pendrive.jpg', '1', '200', 1, 8, 'Premium, reliable and secure storage for your videos, music, photos, or other files\r\nProtect your sensitive files - SanDisk SecureAccess software included\r\nPassword protection and 128-bit AES encryption\r\nShips in Certified Frustration-Free Packaging', 'ScanDisk'),
(7, '2019-05-13 06:47:48', '2019-05-13 06:47:48', '2019-05-13 06:47:48', 1, 1, 'Induction Stove', '/image/induction stove.jpg', '1', '360', 1, 5, 'Polished A-grade Crystal Plate Surface; LED Large Screen Display, 4 digits; Built-in Digital 3-Hour Timer setting; Touch Control Panel\r\n8 Power levels from 300 to 1800 Watts: 300w, 500w, 700w, 1000w, 1200w, 1400w, 1600w, 1800w\r\n8 Temperature Settings from 150 to 450 °: 150 °, 200 °, 260 °, 300 °, 350 °, 400 °, 425 °, 450 °\r\nNew plate top pattern design; Included 18-8 10” 3.5 Qt Stainless Steel Pot\r\nCookware Suitability Detection, Overheating Protection function\r\n', 'RoseWill'),
(8, '2019-05-13 07:18:13', '2019-05-13 07:18:13', '2019-05-13 07:18:13', 1, 1, 'Coffee Maker', '/image/coffee maker.jpg', '1', '720', 1, 3, 'Brews multiple K-Cup Pod sizes: (6, 8, 10 oz.) – The most popular K-Cup Pod brew sizes. Use the 6oz brew size to achieve the strongest brew.\r\nLarge 48 oz. Water reservoir: allows you to brew 6+ cups before having to refill, saving you time and simplifying your morning routine. The water reservoir is removable, making it easy to refill whenever you need to.\r\nSimple button controls: just insert a Pod, select your desired brew size, and brew a fresh, great-tasting cup in under a minute. Brew Time: Less than a minute\r\nDescaling: an important part of cleaning your Keurig brewer. This process helps to remove calcium deposits, or scale, that can build up inside a Coffee maker over time. Please refer to our descaling video for step by step instructions.\r\nAuto-off: an auto-off feature is easily programmed to turn off your Coffee maker after it has been idle for two hours, helping to save energy.', 'KEURIG'),
(9, '2019-05-13 07:23:27', '2019-05-13 07:23:27', '2019-05-13 07:23:27', 1, 1, 'Double Door Compact Refrigerator', '/image/fridge1.jpg', '1', '1200', 1, 3, '3.1 cu.ft. total capacity\r\n87 Liters independent freezer section\r\n1 full width tempered glass shelf for storage, steel door\r\nIntegrated door shelving with tall bottle storage\r\nDimensions 18.87 x 19.68 x 33.43 Inches', 'LG');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bhavya', 'bhavya@gmail.com', NULL, '$2y$10$vDJE71LQTm38cgTnOsO9HeoyL1XBj2UqUm0tpw82ju6OOdIVZQEbG', NULL, '2019-05-24 02:52:12', '2019-05-24 02:52:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_copy`
--
ALTER TABLE `products_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products_copy`
--
ALTER TABLE `products_copy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
