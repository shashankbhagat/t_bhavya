@extends('layouts.app0')


@section('contents')
<table class="table table-light">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Image</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php 
      $count = 1;
      $total = 0;
    ?>
    @foreach($cartProductIds as $key => $value)
    <tr>
      <th scope="row">{{$count}}</th>
      <td><img src="{{$products[$value]['image']}}" height="25px" width="25px"> </td>
      <td>{{$products[$value]['name']}}</td>
      <td>{{$products[$value]['mrp']}}</td>
    </tr>
    <?php 
      $count = $count + 1; 
      $total = $total + $products[$value]['mrp'];
    ?>
  @endforeach
  </tbody>
</table>

<div class="row float-right">
  <div class="">
    <div class="alert alert-primary" role="alert">
      <a class="alert-link">Total :</a> {{$total}}
    </div>
    @if($order['status'] == 'Unpaid')
    <button class="btn btn-primary" type="button">Pay with Razorpay</button>
    @endif
  </div>
</div>
<div class="row">
    <div class="alert alert-primary" role="alert">
      <a class="alert-link">Status :</a> {{$order['status']}}
    </div>
    <!-- <button type="submit" class="btn btn-primary" type="button">Place Order</button> -->
</div>
</table>
<br>
<br>
<br>
@endsection
