@extends('layouts.app0')


@section('contents')
@if($cartCount == 0)
  You have not added any product to cart
@else

<form action="/confirmOrder" method="post">
  @csrf
  <input type="hidden" name="product_json" value="{{$json}}">
<table class="table table-light">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Image</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col">Quantity</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php
      $count = 1;
      $total = 0;
    ?>
    @foreach($cartProducts as $key => $cproducts)
    <tr>
      <th scope="row">{{$count}}</th>
      <td><img src="{{$products[$cproducts['product_id']]['image']}}" height="25px" width="25px"> </td>
      <td>{{$products[$cproducts['product_id']]['name']}}</td>
      <td>{{$products[$cproducts['product_id']]['mrp']}}</td>
      <td>1</td>
      <td><a href="/cart/deleteFromCart/{{$cproducts['id']}}"><i class="fas fa-trash" style="color:red;"></i></a></td>
    </tr>
    <?php
      $count = $count + 1;
      $total = $total + $products[$cproducts['product_id']]['mrp'];
    ?>
  @endforeach
  </tbody>
</table>

<div class="row float-right">
  <div class="">
    <div class="alert alert-primary" role="alert">
      <a class="alert-link">Total :</a> {{$total}}
      <input type="hidden" name="amount" value="{{$total}}">
    </div>
    <button type="submit" class="btn btn-primary">Place Order</button>
  </div>
</div>
</table>
</form>

@endif

<br>
<br>
<br>
@endsection
