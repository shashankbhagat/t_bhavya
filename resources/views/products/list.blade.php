@extends('layouts.app0')

@section('contents')
<style>
div.card {
   /*width: 250px;*/
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
   /*text-align: center;*/
   }
</style>
<div class="row">
  @foreach($products as $product)
  <a href="/product/details/{{$product->id}}" style="text-decoration:none;">
    <div class="col-md-4" >
      <div class="card" style="width: 18rem;">
        <img src="{{$product->image}}" width="400" height="180" class="card-img-top" alt="...">
        <!-- <img src="https://via.placeholder.com/400x200.png" class="card-img-top" alt="..."> -->
        <div class="card-body" style="   ">
          <div class="product-name">
            {{$product->name}}
          </div>
          <div class="small-descrition">
            {{$product->brand}}
          </div>
          <div class="price-tag">
            {{$product->mrp}}
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
  </a>
  @endforeach
</div>


@endsection
