
@extends('layouts.app0')

@section('customStyles')
<style>

.redButton {

}
.name {
    color: #0066c0;
    font-size: 17px!important;
    line-height: 1.255!important;
    font-family: Arial,sans-serif;
}

.discount-label {
  font-weight: 200;
}

.price-value {
  color: #B12704!important;
  font-weight: 500;
}
.lh32 {
  line-height: 32px;
}

.dis {
  color: #008a00!important;
}

.brand-label {
  display: inline;
  color: #888;
  font-weight: 400;
}

.buy{
  background: #ff9f00;
  box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
  border: none;
    color: #fff;
}

.add  {
  background: #fb641b;
    box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
    border: none;
    color: #fff;
}
</style>

@endsection

@section('contents')
<div class="row">
  <div class="col-md-7" style="">
    <img src="{{$product->image}}" height="300px" width="50%" />
  </div>
  <div class="col-md-5 lh32">
    <div class="font-weight-bold name">
      {{$product->name}} <br>
    </div>
    <hr>
    <div class="">
      <span class="price-label">Price :</span>
      <span class="price-value">Rs. {{$product->mrp}}</span>
    </div>
    <div class="">
      <span class="discount-label">Discount :</span>
      <span class="discount-value dis">Rs. {{$product->discount}}</span>
    </div>
    <div class="">
    <span class="inStock-label">InStock :</span>
    <span class="inStock-value">{{$product->inStock}}</span>
    </div>
  <div class="">
    <span class="quantity-label">Quantity :</span>
    <span class="quantity-value">{{$product->quantity}}</span>
  </div>
    <div class="">
      <span class="brand-label">Brand Name :</span>
      <span class="brand-name">{{$product->brand}}</span>
    </div>
    <div class="" style="margin-top:20px;">
      <a href="/cart/addToCart/{{$product->id}}"><button type="button" class="btn btn-danger redButton add"> <i class="fas fa-cart-plus"></i> Add to Cart</button></a>
      <!-- <button type="button" class="btn btn-danger redButton add"> <i class="fas fa-cart-plus"></i> Add to Cart</button> -->
      <!-- <button type="button" class="btn btn-danger buy">Buy Now</button> -->
    </div>
  </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <span class="font-weight-bold">Description</span> : <br>
        <div class="clearfix">

        </div>
        {{$product->description}}
      </div>
    </div>
    @endsection
