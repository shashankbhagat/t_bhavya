@extends('layouts.app0')

@section('contents')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
         @foreach($orders as $order)

         <a href="/viewOrder/{{$order->bill_no}}" style="text-decoration: none;">
            <div class="card">
               <div class="card-header">
                 @if($order->payment_id)
                 <span class="">Payment ID : </span>
                 <span class="">{{$order->payment_id}}</span>
                 @endif
                  <div class="float-right"><i class="fas fa-clock"></i> {{$order->created_at}} </div>
               </div>
               <div class="card-body">
                  <div class="">
                     <span class="">Amount : </span>
                     <span class="">{{$order->amount}}</span>
                  </div>
                  <div class="">
                     <span class="">Status : </span>
                     <span class="">{{$order->status}}</span>
                   </div>


               </div>
            </div>
         </a>
         <br><br>
         @endforeach
      </div>
   </div>
</div>
@endsection
