@extends('layouts.app0')


@section('contents')
<br>
<br>
<br>

<div class="panel-body" style="border: 1px solid #ddd;padding: 10px;background: #eee;width: 30%;">
  <form id="rzp-footer-form" action="{!!route('dopayment')!!}" method="POST" style="width: 100%; text-align: center" >
    @csrf
    <!-- table srts here -->
    <table class="table table-light">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Image</th>
          <th scope="col">Name</th>
          <th scope="col">Price</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $count = 1;
        $total = 0;
        ?>
        @foreach($cartProductIds as $key => $value)
        <tr>
          <th scope="row">{{$count}}</th>
          <td><img src="{{$products[$value]['image']}}" height="25px" width="25px"> </td>
          <td>{{$products[$value]['name']}}</td>
          <td>{{$products[$value]['mrp']}}</td>
        </tr>
        <?php
        $count = $count + 1;
        $total = $total + $products[$value]['mrp'];
        ?>
        @endforeach
      </tbody>
    </table>

    <div class="row float-right">
      <div class="">
        <div class="alert alert-primary" role="alert">
          <a class="alert-link">Total :</a> {{$total}}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="alert alert-primary" role="alert">
        <a class="alert-link">Status :</a> <span id="bill-status">{{$order['status']}}</span>
      </div>
      <!-- <button type="submit" class="btn btn-primary" type="button">Place Order</button> -->
    </div>
  </table>
  <!-- table ends here -->
  <br/>
  <p><br/>Price: {{$total}} INR </p>
  <input type="hidden" name="amount" id="amount" value="{{$total}}"/>
  @if($order['status'] == 'Unpaid')
  <!-- <button class="btn btn-primary" type="button">Pay with Razorpay</button> -->
  <div class="pay" id="pay-button">
    <button class="btn-primary razorpay-payment-button btn filled small" id="paybtn" type="button">Pay with Razorpay</button>
  </div>
  @endif
</form>
<br/><br/>
@if($order['payment_id'])
<div id="paymentDetail">
  <center>
    <div>paymentID: {{$order['payment_id']}}</div>
    <div>paymentDate: {{$order['payment_date']}}</div>
  </center>
</div>
</div>
@endif
<div id="paymentDetail" style="display: none">
  <center>
    <div>paymentID: <span id="paymentID"></span></div>
    <div>paymentDate: <span id="paymentDate"></span></div>
  </center>
</div>
</div>

<br>
<br>
<br>

@endsection

@section('afterScripts')
<script>
  $('#rzp-footer-form').submit(function (e) {
    var button = $(this).find('button');
    var parent = $(this);
    button.attr('disabled', 'true').html('Please Wait...');
    $.ajax({
      method: 'get',
      url: this.action,
      data: $(this).serialize(),
      complete: function (r) {
        console.log('complete');
        console.log(r);
      }
    })
    return false;
  })
</script>

<script>
  function padStart(str) {
    return ('0' + str).slice(-2)
  }

  function demoSuccessHandler(transaction) {
    // You can write success code here. If you want to store some data in database.
    var paymentDate = new Date();
    var datei = padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes());

    $.ajax({
      type : 'get',
      url : '/changeBillStatusToPaid/{{$order["bill_no"]}}/'+transaction.razorpay_payment_id+'/',
      success : function(data) {
        $('#bill-status').text('Paid');
        $('#pay-button').remove();
        $.simplyToast('Payment Received', 'success');
      }
    });
    $("#paymentDetail").removeAttr('style');
    $('#paymentID').text(transaction.razorpay_payment_id);
    $('#paymentDate').text(datei);



    $.ajax({
      method: 'post',
      url: "{!!route('dopayment')!!}",
      data: {
        "_token": "{{ csrf_token() }}",
        "razorpay_payment_id": transaction.razorpay_payment_id
      },
      complete: function (r) {
        console.log('complete');
        console.log(r);
      }
    })
  }
</script>
<script>
  var options = {
    key: "{{ env('RAZORPAY_KEY') }}",
    amount: '{{$total*100}}',
    name: 'CodesCompanion',
    description: 'TVS Keyboard',
    image: 'https://i.imgur.com/n5tjHFD.png',
    handler: demoSuccessHandler
  }
</script>
<script>
  window.r = new Razorpay(options);
  document.getElementById('paybtn').onclick = function () {
    r.open()
  }
</script>

@endsection
